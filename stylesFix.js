const fs = require('fs');

const loginFolder = './node_modules/apostrophe/lib/modules/apostrophe-login';
const adminBarFolder = './node_modules/apostrophe/lib/modules/apostrophe-admin-bar';
const colorsFile = './node_modules/apostrophe/lib/modules/apostrophe-ui/public/css/global/colors.less';
const adminBarStylesFile = './node_modules/apostrophe/lib/modules/apostrophe-ui/public/css/components/admin-bar.less';

const aposLoginOverwrites = fs.readFileSync('./apos-login-overwrites.less', 'utf-8');
const aposAdminBarStylesOverwrites = fs.readFileSync('./admin-bar-overwrites.less', 'utf-8');
const logoSquare = fs.readFileSync('./logo-erp-c.svg', 'utf-8');
const logoFull = fs.readFileSync('./logo-erp.svg', 'utf-8');
const mainColor = '#3A70B5';

async function main () {
  const colorsContent = fs.readFileSync(colorsFile, 'utf-8');
  fs.writeFileSync(colorsFile, colorsContent.replace(/@apos-green: #\w+/, `@apos-green: ${mainColor}`));

  const loginPage = fs.readFileSync(`${loginFolder}/views/loginBase.html`, 'utf-8');
  fs.writeFileSync(`${loginFolder}/views/loginBase.html`, loginPage.replace('{% include "apostrophe-admin-bar:logo.html" %}', logoFull));

  fs.writeFileSync(`${adminBarFolder}/views/logo.html`, logoSquare);

  let loginStyles = fs.readFileSync(`${loginFolder}/public/css/always.less`, 'utf-8');
  loginStyles = loginStyles.replace(/background-color: @apos-primary;/g, 'background-color: #fff;');
  loginStyles = loginStyles.replace(/margin-bottom: @apos-margin-5;/g, 'margin-bottom: 30px;');
  loginStyles = loginStyles.replace(/svg { width: 84px; height: 84px; }/g, 'svg { height: 100px; }');
  loginStyles = loginStyles.replace(/background-color: @apos-light;/g, 'background-color: #5184ce;color:#fff;');
  loginStyles += aposLoginOverwrites;

  fs.writeFileSync(`${loginFolder}/public/css/always.less`, loginStyles);

  let adminBarStyles = fs.readFileSync(adminBarStylesFile, 'utf-8');
  adminBarStyles = adminBarStyles.replace('background-color: @apos-base;', 'background-color: #fff;');
  adminBarStyles = adminBarStyles.replace('color: @apos-white;', 'color: @apos-base;');
  adminBarStyles = adminBarStyles.replace('background-color: darken(@apos-primary, 3%)', 'background-color: darken(@apos-primary, 3%);color:#fff;');
  adminBarStyles = adminBarStyles.replace(/max-height: 30px;\s+max-width: 30px;/, `max-height: 45px;max-width: 45px;width: 100%;height: 100%;`);
  adminBarStyles = adminBarStyles.replace(/(cursor: pointer;)/g, '$1 font-weight:400;');
  adminBarStyles = adminBarStyles.replace(/max-height: 36px;\s+max-width: 36px;/g, 'max-height: 55px;max-width: 55px;');
  adminBarStyles = adminBarStyles.replace(/padding: @apos-padding-2;/g, 'padding: 10px;');
  adminBarStyles = adminBarStyles.replace('background-color: darken(@apos-base, 10%);', '');
  adminBarStyles = adminBarStyles.replace('padding: 13px 15px 8px 15px;', `display: inline-flex;
    text-align: center;
    align-items: center;
    justify-content: center;
    padding: 5px;
    height: 100%;`);
  adminBarStyles += aposAdminBarStylesOverwrites;

  fs.writeFileSync(adminBarStylesFile, adminBarStyles);
}

main();
