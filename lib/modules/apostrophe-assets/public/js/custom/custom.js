$(function () {
  $(window).on('scroll', function () {
    $('.custom-header').toggleClass('custom-header--scroll', $(this).scrollTop() > $(window).height());
  });
});
