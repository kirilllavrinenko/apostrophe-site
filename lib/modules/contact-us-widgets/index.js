module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Contact Us',
  addFields: [
    {
      name: 'image',
      label: 'Image',
      type: 'singleton',
      widgetType: 'apostrophe-images',
      options: {
        limit: 1
      }
    }
  ]
};
