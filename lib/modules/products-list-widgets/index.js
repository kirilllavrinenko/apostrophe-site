module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Products List',
  addFields: [
    {
      name: 'count',
      label: 'Number of categories',
      type: 'integer'
    },
    {
      contextual: true,
      name: 'category0',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category1',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category2',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category3',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category4',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category5',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category6',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category7',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category8',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category9',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category10',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category11',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category12',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category13',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category14',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category15',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category16',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category17',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category18',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category19',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category20',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category21',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category22',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category23',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
    {
      contextual: true,
      name: 'category24',
      label: 'Product Category',
      type: 'area',
      options: {
        widgets: {
          'products-list-category': {}
        }
      }
    },
  ]
};
