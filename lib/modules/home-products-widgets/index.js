module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Add Products Block',
  addFields: [
    {
      name: 'lines',
      label: 'Number of lines',
      type: 'integer'
    },
    {
      contextual: true,
      name: 'block_product_type_00',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_01',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_02',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_03',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_10',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_11',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_12',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_13',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_20',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_21',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_22',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_23',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_30',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_31',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_32',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_33',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_40',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_41',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_42',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_43',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_50',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_51',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_52',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_53',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_60',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_61',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_62',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    },
    {
      contextual: true,
      name: 'block_product_type_63',
      label: 'Product Type',
      type: 'area',
      options: {
        widgets: {
          'home-product-item': {}
        }
      }
    }
  ]
};
