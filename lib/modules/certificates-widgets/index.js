module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Add Quality',
  addFields: [
    {
      type: 'string',
      name: 'name',
      label: 'Name',
      required: true
    },
    {
      type: 'attachment',
      name: 'attachment',
      label: 'File',
      required: true
    }
  ]
};
