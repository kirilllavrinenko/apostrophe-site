module.exports = {
  label: 'Single Image',
  addFields: [
    {
      name: 'image',
      label: 'Image',
      type: 'singleton',
      widgetType: 'apostrophe-images',
      options: {
        limit: 1
      }
    },
    {
      name: 'caption',
      label: 'Caption',
      type: 'string'
    },
    {
      name: 'background',
      label: 'Background',
      type: 'select',
      def: 'none',
      choices: [
        { label: 'None', value: 'none' },
        { label: 'Light', value: 'o-background-light'}
      ]
    }
  ]
};
