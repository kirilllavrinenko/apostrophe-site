const _ = require('lodash');
let baseToolbar = [ 'Styles', 'Bold', 'Italic', 'Blockquote', 'Link', 'Anchor', 'Unlink', 'NumberedList', 'BulletedList', 'Split' ];
let baseStyles = [
  // { name: 'Body Copy (P)', element: 'p', attributes: { class: 'o-body' } },
  { name: 'Section Header (H2)', element: 'h2', attributes: { class: 'o-section-header' } },
  { name: 'Headline Blue (H3)', element: 'h3', attributes: { class: 'o-headline custom-color-blue' } },
  { name: 'Headline (H3)', element: 'h3', attributes: { class: 'o-headline' } },
  { name: 'Sub Headline (H4)', element: 'h4', attributes: { class: 'o-subheadline' } },
  { name: 'Meta (P)', element: 'p', attributes: { class: 'o-meta' } },
  { name: 'Slider (H1)', element: 'h1', attributes: { class: 'font-h1 font-poppins' } },
  { name: 'Slider blue (H3)', element: 'h3', attributes: { class: 'font-h3 font-normal font-dark-blue' } },
  { name: 'H1 Blue', element: 'h1', attributes: { class: 'font-h1 font-dark-moderate-blue font-poppins' } },
  { name: 'H3 Blue', element: 'h3', attributes: { class: 'font-h3 font-dark-moderate-blue font-poppins' } },
  { name: 'P Deep Blue', element: 'p', attributes: { class: 'font-h4 font-roboto font-deep-blue' } }
];
let narrowWidgets = {
  'apostrophe-rich-text': {
    toolbar: baseToolbar,
    styles: baseStyles
  },
  'image': {},
  'slideshow': {},
  'logo-mask': {},
  'link': {},
  'columns': {
    controls: {
      position: 'bottom-left'
    }
  },
  'apostrophe-video': {},
  'artworks': {},
  'locations': {},
  'content': {},
  'articles': {},
  'events': {},
  'random-met-artwork': {}
};

let wideWidgets = {
  'marquee': {},
  'feature': {},
  'two-panel': {},
  'timeline': {},
  'contact-us': {},
  'home-products': {}
};

module.exports = {
  baseToolbar: baseToolbar,
  baseStyles: baseStyles,
  baseWidgets: _.extend({}, narrowWidgets, wideWidgets),
  narrowWidgets: narrowWidgets
};
