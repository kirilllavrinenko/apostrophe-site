module.exports = {
  sanitizeHtml: {
    allowedClasses: {
      'p': ['o-body', 'o-meta', 'font-roboto', 'font-deep-blue', 'font-h4'],
      'h2': ['o-section-header'],
      'h3': ['o-headline', 'font-h3', 'font-normal', 'font-dark-moderate-blue', 'font-dark-blue', 'font-blue', 'font-poppins'],
      'h4': ['o-subheadline'],
      'h1': ['font-h1', 'font-dark-moderate-blue', 'font-blue', 'font-poppins']
    },
    allowedTags: [
      'h1', 'h2', 'h3', 'h4', 'p', 'a', 'ul', 'ol', 'li', 'strong', 'em', 'blockquote'
    ],
    allowedSchemes: ['http', 'https', 'ftp', 'mailto', 'tel']
  }
};
