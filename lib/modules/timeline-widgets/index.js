module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Timeline',
  addFields: [
    {
      name: 'date',
      label: 'Date',
      type: 'string'
    },
    {
      name: 'text',
      label: 'Text',
      type: 'string'
    }
  ]
};
