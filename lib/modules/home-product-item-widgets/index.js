module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Add Products Type',
  addFields: [
    {
      name: 'image',
      label: 'Product Type Image',
      type: 'singleton',
      widgetType: 'apostrophe-images',
      controls: {
        position: 'top-right'
      },
      options: {
        aspectRatio: [ 1, 1 ],
        minSize: [ 28, 28 ],
        maxSize: [ 32, 32 ],
        limit: 1
      }
    },
    {
      name: 'name',
      label: 'Type',
      type: 'string'
    }
  ]
};
